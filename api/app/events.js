const express = require('express');
const Event = require('../models/Event');
const auth = require('../middleware/auth');
const moment = require('moment');
const User = require("../models/User");

const router = express.Router();

router.get('/', auth, async (req, res) => {
	try {
		const invitations = await Event.find({friends: {$in: req.user._id.toString()}}).sort('date');

		const events = await Event.find({user: req.user._id}).populate('friends', 'displayName').sort('date');

		if (events.length === 0) {
			return res.status(404).send({error: 'Events not found'});
		}

		const today = moment().format("YYYY-MM-DD");

		const totalEvents = events.concat(invitations);
		const sortByDate = totalEvents.sort((a, b) => moment(`${a}.date`, 'DD-MM-YYYY').diff(moment(`${b}.date`, 'DD-MM-YYYY')));
		const outputs = sortByDate.filter(event => event.date >= today);

		res.send(outputs);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.get('/:id', async (req, res) => {
	try {
		const event = await Event.findById(req.params.id).populate('friends', 'displayName');

		if (!event) {
			return res.status(404).send({error: 'Event not found'});
		}

		res.send(event);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.post('/', auth, async (req, res) => {
	try {
		const eventData = {
			user: req.user._id,
			date: req.body.date,
			text: req.body.text,
			duration: req.body.duration,
		};

		const newEvent = new Event(eventData);
		await newEvent.save();
		res.send(newEvent);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.post('/addFriend', async (req, res) => {
	try {
		const query = {};

		if (req.query.event) {
			query.event = req.query.event;
		}

		const event = await Event.findById(query.event);

		// if (!event) {
		// 	return res.status(404).send({error: 'Event not found'});
		// }

		const user = await User.findOne({email: req.body.email});

		if (!user) {
			return res.status(404).send({error: 'User not found'});
		}

		event.friends.push(user._id);
		event.save();
		res.send({message: 'Added'});
	} catch (error) {
		res.sendStatus(500);
	}
});

router.post('/deleteFriend', async (req, res) => {
	try {
		const query = {};

		if (req.query.event) {
			query.event = req.query.event;
		}

		const event = await Event.findById(query.event);

		const user = await User.findById(req.body.user);

		if (!user) {
			return res.status(404).send({error: 'User not found'});
		}

		const index = event.friends.findIndex(p => p.toString() === req.body.user);
		event.friends.splice(index, 1);

		event.save();
		res.send({message: 'Deleted'});
	} catch (error) {
		res.sendStatus(500);
	}
});

router.put('/:id', auth, async (req, res) => {
	try {
		const event = await Event.findById(req.params.id);

		if (!event) {
			return res.status(404).send({error: 'Event not found'});
		}

		if (req.user._id.toString() !== event.user._id.toString()) {
			return res.status(403).send({error: 'Permissions denied'});
		}

		const newEventData = {
			date: req.body.date,
			text: req.body.text,
			duration: req.body.duration,
		};

		const updateEvent = await Event.findByIdAndUpdate(req.params.id, newEventData, {
			new: true,
			runValidators: true,
		});

		res.send(updateEvent);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.delete('/:id', auth, async (req, res) => {
	const event = await Event.findById(req.params.id);

	// if (!event) {
	// 	return res.status(404).send({error: 'Event not found'});
	// } ?

	if (req.user._id.toString() !== event.user._id.toString()) {
		return res.status(403).send({error: 'Permissions denied'});
	}

	try {
		await Event.findByIdAndRemove(req.params.id);
		res.send({message: 'Event removed'});
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;