const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  await User.create({
    email: 'admin@gmail.com',
    password: 'admin',
    token: nanoid(),
	  role: 'admin',
	  displayName: 'Admin',
  }, {
    email: 'user@gmail.com',
    password: 'user',
    token: nanoid(),
	  role: 'user',
	  displayName: 'User',
  });

  await mongoose.connection.close();
};

run().catch(console.error);