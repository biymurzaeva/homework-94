const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');
const moment = require("moment");

const validateDate = value => {
	const today = moment().format('YYYY-MM-DD');
	const inputDate = moment(value, "YYYY-MM-DD").format('YYYY-MM-DD');

	if (inputDate < today) return false;
};

const validateInputDate = value => {
	const inputDate = moment(value, "YYYY-MM-DD").isValid();

	if (!inputDate) return false;
};

const validateFormatDate = value => {
	const defaultFormat = /^(\d{4})([-]\d{1,2}){2}$/;
	if (!defaultFormat.test(value)) return false;
};

const EventSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	date: {
		type: String,
		validate: [
			{validator: validateFormatDate, message: 'Wrong date format'},
			{validator: validateInputDate, message: 'Invalid date'},
			{validator: validateDate, message: 'Input date must be greater than or equal to the current date!'},
		],
		required: true,
	},
	text: {
		type: String,
		required: true,
	},
	duration: {
		type: String,
		required: true
	},
	friends: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
	}],
});

EventSchema.plugin(idvalidator);

const Event = mongoose.model('Event', EventSchema);
module.exports = Event;