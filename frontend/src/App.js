import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Events from "./containers/Events/Events";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddEvent from "./containers/AddEvent/AddEvent";
import InviteForm from "./components/InviteForm/InviteForm";
import List from "./containers/List/List";
import EditEvent from "./containers/EditEvent/EditEvent";

const App = () => {
	return (
		<Layout>
			<Switch>
				<Route path="/" exact component={Events}/>
				<Route path="/register" component={Register}/>
				<Route path="/login" component={Login}/>
				<Route path="/add" component={AddEvent}/>
				<Route path="/addFriend" component={InviteForm}/>
				<Route path="/list-friends/event/:id" component={List}/>
				<Route path="/edit/event/:id" component={EditEvent}/>
			</Switch>
		</Layout>
	);
};

export default App;
