import React from 'react';
import {Link} from "react-router-dom";
import {Box, Button, makeStyles, Paper} from "@material-ui/core";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import ListIcon from '@material-ui/icons/List';
import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles({
	content: {
		display: "flex",
		flexWrap: "wrap",
		justifyContent: "space-between",
		alignItems: "center",
	},
	button: {
		margin: "0 10px"
	},
});

const Event = ({id, date, text, duration, deleteHandler}) => {
	const classes = useStyles();

	return (
		<Paper component={Box} m={3} p={2}>
			<div className={classes.content}>
				<div>
					Date: {date} <strong>{text}</strong> Duration: {duration}
				</div>
				<div>
					<Button variant="outlined" color="secondary" className={classes.button} onClick={deleteHandler}>
						<DeleteOutlineIcon/>
					</Button>
					<Button variant="outlined" color="primary" className={classes.button} component={Link} to={`/addFriend?event=${id}`}>
						<MailOutlineIcon/>
					</Button>
					<Button variant="outlined" color="primary" className={classes.button} component={Link} to={`/list-friends/event/${id}`}>
						<ListIcon/>
					</Button>
					<Button variant="outlined" color="primary" className={classes.button} component={Link} to={`/edit/event/${id}`}>
						<EditIcon/>
					</Button>
				</div>
			</div>
		</Paper>
	);
};

export default Event;