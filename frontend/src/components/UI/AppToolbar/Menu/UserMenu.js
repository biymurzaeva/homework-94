import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import path from "path";
import {Button, Grid, makeStyles, Menu, MenuItem} from "@material-ui/core";
import defaultAvatar from "../../../../assets/images/default_avatar.png";
import {apiURL} from "../../../../config";
import {logoutUser} from "../../../../store/actions/usersActions";

const useStyles = makeStyles({
	avatar: {
		width: '35px',
		height: '35px',
		borderRadius: '50%'
	},
});

const UserMenu = ({user}) => {
	const classes = useStyles();
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

	let avatar = defaultAvatar;
	const extensions = ['.png', '.jpg', 'jpeg', '.svg', '.gif'];

	if (user.avatarImage) {
		if (extensions.includes(path.extname(user.avatarImage))) {
			avatar = apiURL + '/' + user.avatarImage;
		} else {
			avatar = user.avatarImage;
		}
	}

  return (
    <Grid container justifyContent="space-between" alignItems="center">
	    <img src={avatar} alt="Avatar" className={classes.avatar}/>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
        Hello, {user.displayName}!
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
      </Menu>
    </Grid>
  );
};

export default UserMenu;