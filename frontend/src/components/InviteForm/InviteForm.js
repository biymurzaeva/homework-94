import React, {useState} from 'react';
import {Grid, makeStyles} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {addFriends} from "../../store/actions/eventsActions";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

const InviteForm = ({location}) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const loading = useSelector(state => state.events.addFriendsLoading);
	const error = useSelector(state => state.events.addFriendsError);
	const [email, setEmail] = useState('');

	const submitFormHandler = e => {
		e.preventDefault();

		dispatch(addFriends({email: email, id: location.search}));
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	return (
		<Grid item xs={6}>
			<Grid
				container
				direction="column"
				spacing={2}
				component="form"
				className={classes.root}
				autoComplete="off"
				onSubmit={submitFormHandler}
				noValidate
			>
				<FormElement
					required
					type="text"
					label="Email"
					name="email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					error={getFieldError('email')}
				/>
				<Grid item xs={12}>
					<ButtonWithProgress
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						className={classes.submit}
						loading={loading}
						disabled={loading || !email}
					>
						SEND
					</ButtonWithProgress>
				</Grid>
			</Grid>
		</Grid>
	);
};

export default InviteForm;