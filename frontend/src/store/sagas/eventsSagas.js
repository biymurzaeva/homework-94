import {call, put, takeEvery} from "redux-saga/effects";
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {
	addFriends,
	addFriendsFailure,
	addFriendsSuccess,
	createEvent,
	createEventFailure,
	createEventSuccess,
	deleteEvent,
	deleteEventFailure,
	deleteEventSuccess, deleteFriend,
	deleteFriendFailure, deleteFriendSuccess,
	fetchEvent,
	fetchEventFailure,
	fetchEvents,
	fetchEventsFailure,
	fetchEventsSuccess,
	fetchEventSuccess, updateEvent, updateEventFailure, updateEventSuccess
} from "../actions/eventsActions";

export function* createEventSaga({payload: eventData}) {
	try {
		yield axiosApi.post('/events', eventData);
		yield put(createEventSuccess());
		yield call(historyPush('/'));
		toast.success('Created new event!');
	} catch (error) {
		yield put(createEventFailure(error.response.data));
		toast.error(error.response.data.global);
	}
}

export function* fetchEventsSaga() {
	try {
		const events = yield axiosApi.get('/events');
		yield put(fetchEventsSuccess(events.data));
	} catch (error) {
		yield put(fetchEventsFailure(error.response.data));
	}
}

export function* deleteEventSaga({payload: id}) {
	try {
		yield axiosApi.delete(`/events/${id}`)
		yield put(deleteEventSuccess(id));
		toast.success('Event deleted');
	} catch (error) {
		yield put(deleteEventFailure(error.response.data));
	}
}

export function* addFriendsSaga({payload: data}) {
	try {
		yield axiosApi.post(`/events/addFriend${data.id}`, {email: data.email});
		yield put(addFriendsSuccess());
		yield call(historyPush('/'));
		toast.success('User invited');
	} catch (error) {
		yield put(addFriendsFailure(error.response.data));
		toast.error(error.response.data.error);
	}
}

export function* fetchEventSaga({payload: id}) {
	try {
		const response = yield axiosApi.get(`/events/${id}`);
		yield put(fetchEventSuccess(response.data));
	} catch (error) {
		yield put(fetchEventFailure(error.response.data));
	}
}

export function* deleteFriendSaga({payload: data}) {
	try {
		const response = yield axiosApi.post(`/events/deleteFriend?event=${data.id}`, {user: data.user});
		yield put(deleteFriendSuccess(data.user));
		toast.success(response.data.message);
	} catch (error) {
		yield put(deleteFriendFailure(error.response.data));
		toast.error(error.response.data.error);
	}
}

export function* updateEventSaga({payload: data}) {
	try {
		yield axiosApi.put(`/events/${data.id}`, data.eventData);
		yield put(updateEventSuccess());
		yield call(historyPush('/'));
		toast.success('Event updated');
	} catch (error) {
		yield put(updateEventFailure(error.response.data));
		toast.error(error.response.data.error);
	}
}

const eventsSagas = [
	takeEvery(createEvent, createEventSaga),
	takeEvery(fetchEvents, fetchEventsSaga),
	takeEvery(deleteEvent, deleteEventSaga),
	takeEvery(addFriends, addFriendsSaga),
	takeEvery(fetchEvent, fetchEventSaga),
	takeEvery(deleteFriend, deleteFriendSaga),
	takeEvery(updateEvent, updateEventSaga),
];

export default eventsSagas