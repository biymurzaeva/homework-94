import {takeEvery, put, call} from "redux-saga/effects";
import {
	registerUser,
	registerUserFailure,
	registerUserSuccess,
	logoutUser,
	loginUserSuccess,
	loginUserFailure, loginUser, googleLogin,
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

export function* registerUserSaga ({payload: userData}) {
	try {
		const response = yield axiosApi.post('/users', userData);
		yield put(registerUserSuccess(response.data));
		yield call(historyPush('/'));
		toast.success('Registration successful');
	} catch (error) {
		yield put(registerUserFailure(error.response.data));
		toast.error(error.response.data.global);
	}
}

export function* logoutUserSaga () {
	try {
		yield axiosApi.delete('/users/sessions');
		yield call(historyPush('/'));
	} catch (error) {
		toast.error('Try again');
	}
}

export function* loginUserSaga({payload: userData}) {
	try {
		const response = yield axiosApi.post('/users/sessions', userData);
		yield put(loginUserSuccess(response.data.user));
		yield call(historyPush('/'));
		toast.success('Login successful');
	} catch (error) {
		yield put(loginUserFailure(error.response.data));
		toast.error(error.response.data.global);
	}
}

export function* googleLoginSaga({payload: googleData}) {
	try {
		const response = yield axiosApi.post('/users/googleLogin',
		{
			tokenId: googleData.tokenId,
			googleId: googleData.googleId,
		});
		yield put(loginUserSuccess(response.data.user));
		yield call(historyPush('/'));
		toast.success('Login successful');
	} catch (error) {
		yield put(loginUserFailure(error.response.data));
		toast.error(error.response.data.global);
	}
}

const usersSaga = [
	takeEvery(registerUser, registerUserSaga),
	takeEvery(loginUser, loginUserSaga),
	takeEvery(logoutUser, logoutUserSaga),
	takeEvery(googleLogin, googleLoginSaga),
];

export default usersSaga;
