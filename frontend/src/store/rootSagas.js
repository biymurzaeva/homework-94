import {all} from 'redux-saga/effects';
import registerUserSaga from "./sagas/usersSagas";
import createEventSaga from "./sagas/eventsSagas";

export function* rootSagas() {
	yield all([
		...registerUserSaga,
		...createEventSaga,
	]);
}