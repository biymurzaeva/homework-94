import myHistory from "../../history";

export const historyPush = payload => {
  return () => {
	  myHistory.push(payload);
  };
};

export const historyReplace = payload => {
  return () => {
	  myHistory.replace(payload);
  };
};