import eventSlices from "../slices/eventSlices";

export const {
	createEvent,
	createEventSuccess,
	createEventFailure,
	fetchEvents,
	fetchEventsSuccess,
	fetchEventsFailure,
	deleteEvent,
	deleteEventSuccess,
	deleteEventFailure,
	addFriends,
	addFriendsSuccess,
	addFriendsFailure,
	fetchEvent,
	fetchEventSuccess,
	fetchEventFailure,
	deleteFriend,
	deleteFriendSuccess,
	deleteFriendFailure,
	updateEvent,
	updateEventSuccess,
	updateEventFailure,
} = eventSlices.actions;