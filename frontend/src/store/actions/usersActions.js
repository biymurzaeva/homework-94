import usersSlice from "../slices/usersSlice";

export const {
	registerUser,
	registerUserFailure,
	registerUserSuccess,
	loginUser,
	loginUserSuccess,
	loginUserFailure,
	logoutUser,
	googleLogin,
	clearErrorUser,
} = usersSlice.actions;
