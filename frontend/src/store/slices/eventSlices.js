const {createSlice}  = require('@reduxjs/toolkit');

const name = 'event';

const eventSlice = createSlice({
	name,
	initialState: {
		createEventLoading: false,
		createEventError: null,
		fetchLoading: false,
		fetchError: null,
		events: [],
		deleteLoading: false,
		deleteError: null,
		addFriendsLoading: false,
		addFriendsError: null,
		event: {},
		fetchEventLoading: false,
		fetchEventError: null,
		deleteFriendLoading: false,
		deleteFriendError: null,
		updateEventLoading: false,
		updateEventError: null,
	},
	reducers: {
		createEvent(state) {
			state.createEventLoading = true;
		},
		createEventSuccess(state) {
			state.createEventLoading = false;
			state.createEventError = null;
		},
		createEventFailure(state, action) {
			state.createEventLoading = false;
			state.createEventError = action.payload;
		},
		fetchEvents(state) {
			state.fetchLoading = true;
		},
		fetchEventsSuccess(state, action) {
			state.fetchLoading = false;
			state.fetchError = null;
			state.events = action.payload;
		},
		fetchEventsFailure(state, action) {
			state.fetchLoading = false;
			state.fetchError = action.payload;
		},
		deleteEvent(state) {
			state.deleteLoading = true;
		},
		deleteEventSuccess(state, action) {
			state.deleteLoading = false;
			state.deleteError = null;
			state.events = state.events.filter(e => e._id !== action.payload);
		},
		deleteEventFailure(state, action) {
			state.deleteLoading = false;
			state.deleteError = action.payload;
		},
		addFriends(state) {
			state.addFriendsLoading = true;
		},
		addFriendsSuccess(state) {
			state.addFriendsLoading = false;
		},
		addFriendsFailure(state, action) {
			state.addFriendsLoading = false;
			state.addFriendsError = action.payload;
		},
		fetchEvent(state) {
			state.fetchEventLoading = true;
		},
		fetchEventSuccess(state, action) {
			state.fetchEventLoading = false;
			state.fetchEventError = null;
			state.event = action.payload;
		},
		fetchEventFailure(state, action) {
			state.fetchEventLoading = false;
			state.fetchEventError = action.payload;
		},
		deleteFriend(state) {
			state.deleteLoading = true;
		},
		deleteFriendSuccess(state, action) {
			state.deleteLoading = false;
			state.deleteError = null;
			state.event = state.event.friends.filter(e => e.toString() !== action.payload);
		},
		deleteFriendFailure(state, action) {
			state.deleteLoading = false;
			state.deleteError = action.payload;
		},
		updateEvent(state) {
			state.updateEventLoading = true;
		},
		updateEventSuccess(state) {
			state.updateEventLoading = false;
			state.updateEventError = null;
		},
		updateEventFailure(state, action) {
			state.updateEventLoading = false;
			state.updateEventError = action.payload;
		},
	}
});

export default eventSlice;
