const {createSlice} = require("@reduxjs/toolkit");

export const initialState = {
	user: null,
	registerLoading: false,
	registerError: null,
	loginLoading: false,
	loginError: null,
};

const name = 'users';

const usersSlice = createSlice({
	name,
	initialState,
	reducers: {
		registerUser(state) {
			state.registerLoading = true;
		},
		registerUserSuccess(state, {payload: userData}) {
			state.user = userData;
			state.registerLoading = false;
			state.registerError = null;
		},
		registerUserFailure(state, action) {
			state.registerLoading = false;
			state.registerError = action.payload;
		},
		logoutUser(state) {
			state.user = null;
		},
		loginUser(state) {
			state.loginLoading = true
		},
		loginUserSuccess(state, action) {
			state.loginLoading = false;
			state.loginError = null;
			state.user = action.payload;
		},
		loginUserFailure(state, action) {
			state.loginLoading = false;
			state.loginError = action.payload;
		},
		googleLogin() {},
		clearErrorUser(state) {
			state.loginError = null;
			state.registerError = null;
		},
	}
});

export default usersSlice;