import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {Router} from "react-router-dom";
import myHistory from "./history";
import {ToastContainer} from "react-toastify";
import App from './App';
import store from "./store/configureStore";
import theme from "./theme";
import {MuiThemeProvider} from "@material-ui/core";

import 'react-toastify/dist/ReactToastify.css';

const app = (
	<Provider store={store}>
		<Router history={myHistory}>
			<MuiThemeProvider theme={theme}>
				<ToastContainer/>
				<App/>
			</MuiThemeProvider>
		</Router>
	</Provider>
);

ReactDOM.render(app, document.getElementById('root'));
