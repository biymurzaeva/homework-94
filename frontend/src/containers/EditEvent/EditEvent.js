import React, {useEffect, useState} from 'react';
import {CircularProgress, Container, Grid, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchEvent, updateEvent} from "../../store/actions/eventsActions";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import Register from "../Register/Register";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

const EditEvent = ({match}) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);
	const error = useSelector(state => state.events.updateEventError);
	const loading = useSelector(state => state.events.updateEventLoading);
	const event = useSelector(state => state.events.event);
	const fetchLoading = useSelector(state => state.events.fetchEventLoading);

	const [newEvent, setNewEvent] = useState(event);

	useEffect(() => {
		dispatch(fetchEvent(match.params.id));
			// setNewEvent(event);
	},[dispatch, match.params.id]);

	const submitFormHandler = e => {
		e.preventDefault();
		dispatch(updateEvent({id: match.params.id, eventData: newEvent}));
	};

	const inputChangeHandler = e => {
		const name = e.target.name;
		const value = e.target.value;
		setNewEvent(prevState => {
			return {...prevState, [name]: value};
		});
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	return user ?
		<Container>
			<Grid container>
				{fetchLoading ?
					<Grid container justifyContent="center" alignItems="center">
						<Grid item>
							<CircularProgress/>
						</Grid>
					</Grid> :
					<Grid item xs={12}>
						<Grid
							container
							direction="column"
							spacing={2}
							component="form"
							className={classes.root}
							autoComplete="off"
							onSubmit={submitFormHandler}
							noValidate
						>

							<FormElement
								required
								label="Date"
								name="date"
								placeholder="year-month-day"
								value={newEvent.date}
								onChange={inputChangeHandler}
								error={getFieldError('date')}
							/>

							<FormElement
								required
								type="text"
								label="Text"
								name="text"
								value={newEvent.text}
								onChange={inputChangeHandler}
								error={getFieldError('text')}
							/>

							<FormElement
								required
								type="text"
								label="Duration"
								name="duration"
								value={newEvent.duration}
								onChange={inputChangeHandler}
								error={getFieldError('duration')}
							/>

							<Grid item xs={12}>
								<ButtonWithProgress
									type="submit"
									fullWidth
									variant="contained"
									color="primary"
									className={classes.submit}
									loading={loading}
									disabled={loading}
								>
									Create
								</ButtonWithProgress>
							</Grid>
						</Grid>
					</Grid>
				}
			</Grid>
		</Container> : <Register to="/register"/>
};

export default EditEvent;