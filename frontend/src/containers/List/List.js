import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteFriend, fetchEvent} from "../../store/actions/eventsActions";
import {Box, Button, CircularProgress, Grid, Paper, Typography} from "@material-ui/core";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

const List = ({match}) => {
	const dispatch = useDispatch();
	const event = useSelector(state => state.events.event);
	const eventLoading = useSelector(state => state.events.fetchEventLoading);
	const eventError = useSelector(state => state.events.fetchEventError);

	useEffect(() => {
		dispatch(fetchEvent(match.params.id));
	},[dispatch, match.params.id]);

	return (
		<>
			<Typography component="h1" variant="h6" color="secondary">List of invitees: </Typography>
			{eventLoading ? <Grid container justifyContent="center" alignItems="center">
				<Grid item>
					<CircularProgress/>
				</Grid>
			</Grid> : eventError ? <Typography color="primary" variant="h5">{eventError.error}</Typography> :
				(event.friends && event.friends.length !== 0) && event.friends.map(e => (
					<Grid item xs={6} key={e._id}>
						<Paper component={Box} m={3} p={2}>
							<Grid item container justifyContent="space-between">
								<Typography variant="subtitle1">{e.displayName}</Typography>
								<Button onClick={() => dispatch(deleteFriend({id: match.params.id, user: e._id}))}>
									<HighlightOffIcon/>
								</Button>
							</Grid>
						</Paper>
					</Grid>
				))
			}
		</>
	);
};

export default List;