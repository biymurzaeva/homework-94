import React, {useEffect, useState} from 'react';
import {Link as RouterLink} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {Avatar, Container, Grid, Link, makeStyles, TextField, Typography} from "@material-ui/core";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {clearErrorUser, registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Register = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const error = useSelector(state => state.users.registerError);
  const loading = useSelector(state => state.users.registerLoading);

  const [user, setUser] = useState({
	  email: '',
    password: '',
	  displayName: '',
	  avatarImage: null,
  });

  useEffect(() => {
  	return () => {
  		dispatch(clearErrorUser());
	  };
  }, [dispatch]);

  const inputChangeHandler = e => {
    const {name, value} = e.target;
    setUser(prevState => ({...prevState, [name]: value}));
  };

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setUser(prevState => {
			return {...prevState, [name]: file};
		});
	};

	const submitFormHandler = e => {
    e.preventDefault();

	  const formData = new FormData();

	  Object.keys(user).forEach(key => {
		  formData.append(key, user[key]);
	  });

	  dispatch(registerUser(formData));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  return (
    <Container component="section" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon/>
        </Avatar>
        <Typography component="h1" variant="h6">
          Sign up
        </Typography>
        <Grid
          component="form"
          container
          className={classes.form}
          onSubmit={submitFormHandler}
          spacing={2}
          noValidate
        >
          <FormElement
            type="email"
            required
            autoComplete="new-email"
            label="Email"
            name="email"
            value={user.email}
            onChange={inputChangeHandler}
            error={getFieldError('email')}
          />

          <FormElement
            type="password"
            required
            autoComplete="new-password"
            label="Password"
            name="password"
            value={user.password}
            onChange={inputChangeHandler}
            error={getFieldError('password')}
          />

	        <FormElement
		        type="displayName"
		        required
		        label="Display Name"
		        name="displayName"
		        value={user.displayName}
		        onChange={inputChangeHandler}
		        error={getFieldError('displayName')}
	        />

	        <Grid item xs>
		        <TextField
			        type="file"
			        name="avatarImage"
			        onChange={fileChangeHandler}
			        error={getFieldError('avatarImage')}
		        />
	        </Grid>

	        <Grid item xs={12}>
		        <ButtonWithProgress
			        type="submit"
			        fullWidth
			        variant="contained"
			        color="primary"
			        className={classes.submit}
			        loading={loading}
			        disabled={loading}
		        >
			        Sign up
		        </ButtonWithProgress>
	        </Grid>

          <Grid item container justifyContent="flex-end">
            <Link component={RouterLink} variant="body2" to="/login">
              Already have an account? Sign in
            </Link>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default Register;