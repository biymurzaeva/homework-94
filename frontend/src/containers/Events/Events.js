import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Button, CircularProgress, Container, Grid, Paper, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {deleteEvent, fetchEvents} from "../../store/actions/eventsActions";
import Register from "../Register/Register";
import Event from "../../components/Event/Event";

const Events = () => {
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);
	const events = useSelector(state => state.events.events);
	const loading = useSelector(state => state.events.fetchLoading);
	const error = useSelector(state => state.events.fetchError);

	useEffect(() => {
		dispatch(fetchEvents());
	}, [dispatch]);

	return user ?
		<Container>
			<Grid container>
				<Grid item>
					<Button color="primary" component={Link} to='/add'>Add</Button>
				</Grid>
					{loading ?
						<Grid container justifyContent="center" alignItems="center">
							<Grid item>
								<CircularProgress/>
							</Grid>
						</Grid> :
						<Grid item xs={12}>
							{error ? <Typography component="h1" variant="h6" color="secondary">Friends not found</Typography> :
								events && events.map(event => (
									event.user === user._id ?
									<Event
										key={event._id}
										id={event._id}
										date={event.date}
										text={event.text}
										duration={event.duration}
										deleteHandler={() => dispatch(deleteEvent(event._id))}
									/> :
										<Paper component={Box} m={3} p={2} key={event._id}>
											<div>
												Date: {event.date} <strong>{event.text}</strong> Duration: {event.duration}
											</div>
										</Paper>
								))
							}
						</Grid>
					}
			</Grid>
		</Container> : <Register to="/register"/>
};

export default Events;