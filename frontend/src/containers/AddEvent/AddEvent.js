import React, {useState} from 'react';
import {Container, Grid, makeStyles, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import {createEvent} from "../../store/actions/eventsActions";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

const AddEvent = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);
	const error = useSelector(state => state.events.createEventError);
	const loading = useSelector(state => state.events.createEventLoading);

	const [event, setEvent] = useState({
		date: '',
		text: '',
		duration: '',
	});

	const submitFormHandler = e => {
		e.preventDefault();

		dispatch(createEvent({...event}));
	};

	const inputChangeHandler = e => {
		const name = e.target.name;
		const value = e.target.value;
		setEvent(prevState => {
			return {...prevState, [name]: value};
		});
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	return user ?
		<Container component="section" maxWidth="xs">
			<Grid
				container
				direction="column"
				spacing={2}
				component="form"
				className={classes.root}
				autoComplete="off"
				onSubmit={submitFormHandler}
				noValidate
			>

				<FormElement
					required
					label="Date"
					name="date"
					placeholder="year-month-day"
					value={event.date}
					onChange={inputChangeHandler}
					error={getFieldError('date')}
				/>

				<FormElement
					required
					type="text"
					label="Text"
					name="text"
					value={event.text}
					onChange={inputChangeHandler}
					error={getFieldError('text')}
				/>

				<FormElement
					required
					type="text"
					label="Duration"
					name="duration"
					value={event.duration}
					onChange={inputChangeHandler}
					error={getFieldError('duration')}
				/>

				<Grid item xs={12}>
					<ButtonWithProgress
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						className={classes.submit}
						loading={loading}
						disabled={loading}
					>
						Create
					</ButtonWithProgress>
				</Grid>
			</Grid>
		</Container> :
		<Typography component="h1" variant="h5">Access is denied!</Typography>
};

export default AddEvent;